#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include <stdbool.h>

#include "types.h"

/*************************************************************
 * Mesh management and traversal helpers
 ************************************************************/

/// For a given side s of a triangle, returns the next side t
/// in counter-clockwise order.
static inline face_side_t
next_side(const face_side_t fs)
{
	face_side_t next = {
		.face = fs.face,
		.side = (fs.side + 1) % 3
	};

	return next;
}

/// For a given face-side fs, returns the neighbouring
/// face-side in another triangle.
static inline face_side_t
other(const gluing_map_t G, const face_side_t fs)
{
	face_side_t other = G.data[fs.face][fs.side];

	return other;
}

/*************************************************************
 * Geometric subroutines
 ************************************************************/

/// Computes the area of the face f from edge lengths.
static inline double
face_area(const lengths_array_t l, const uint32_t f)
{
	// Gather edge lengths
	const double l_a = l.data[f][0];
	const double l_b = l.data[f][1];
	const double l_c = l.data[f][2];

	// Heron's rule
	const double s = 0.5 * (l_a + l_b + l_c);
	const double d = s * (s - l_a) * (s - l_b) * (s - l_c);
	const double area = sqrt(d);

	return area;
}

/// Compute the surface area of a triangulation.
static inline double
surface_area(const adjacency_list_t F, const lengths_array_t l)
{
	double total_area = 0.0;

	for (uint32_t f = 0; f < F.face_count; ++f)
	{
		total_area += face_area(l, f);
	}

	return total_area;
}

/// Computes triangle corner angle opposite the face-side fs.
static double
opposite_corner_angle(const lengths_array_t l, const face_side_t fs)
{
	// Gather edge lengths
	const face_side_t a = fs;
	const face_side_t b = next_side(a);
	const face_side_t c = next_side(b);

	const double l_a = l.data[a.face][a.side];
	const double l_b = l.data[b.face][b.side];
	const double l_c = l.data[c.face][c.side];

	// Law of cosines
	const double d = ((l_b * l_b) + (l_c * l_c) - (l_a * l_a)) / (2 * l_b * l_c);
	const double angle_in_radians = acos(d);

	return angle_in_radians;
}

/// Computes the length of the opposite diagonal of the diamond formed by the
/// triangle containing fs, and the neighboring triangle adjacent to fs.
/// This is the new edge length needed when flipping the edge fs.
static double
diagonal_length(const gluing_map_t G, const lengths_array_t l,
				const face_side_t fs)
{
	// Gather lengths and angles
	const face_side_t fs_opp = other(G, fs);
	const face_side_t fs_next_next = next_side(next_side(fs));
	const face_side_t fs_opp_next = next_side(fs_opp);

	const double u = l.data[fs_next_next.face][fs_next_next.side];
	const double v = l.data[fs_opp_next.face][fs_opp_next.side];

	const double θa = opposite_corner_angle(l, next_side(fs));
	const double θb = opposite_corner_angle(l, next_side(next_side(fs_opp)));

	const double d = (u * u) + (v * v) - (2.0 * u * v * cos(θa + θb));
	const double length = sqrt(d);

	return length;
}

/// Test if the edge given by face-side fs satisfies the intrinsic
/// Delaunay property.
static bool
is_delaunay(const gluing_map_t G, const lengths_array_t l,
			const face_side_t fs)
{
	const face_side_t fs_opp = other(G, fs);

	const double θa = opposite_corner_angle(l, fs);
	const double θb = opposite_corner_angle(l, fs_opp);

	const double epsilon = 1e-5;
	return θa + θb <= M_PI + epsilon;
}

/*************************************************************
 * Construct initial data
 ************************************************************/

static lengths_array_t
build_edge_lengths(const vertices_t V, const adjacency_list_t F)
{
	lengths_array_t l = {
		.data = calloc(F.face_count * 3ull, sizeof(double)),
		.face_count = F.face_count
	};

	for (uint32_t f = 0; f < F.face_count; ++f)
	{
		for (uint32_t s = 0; s < 3; ++s)
		{
			const face_side_t fs = { .face = f, .side = s };
			const face_side_t fs_next = next_side(fs);

			const uint32_t i = F.data[fs.face][fs.side];
			const uint32_t j = F.data[fs_next.face][fs_next.side];
			const vertex_t v = {
				.x = V.data[j].x - V.data[i].x,
				.y = V.data[j].y - V.data[i].y,
				.z = V.data[j].z - V.data[i].z
			};
			const double d = (v.x * v.x) + (v.y * v.y) + (v.z * v.z);
			const double length = sqrt(d);

			l.data[fs.face][fs.side] = length;
		}
	}

	return l;
}

static int
sort_rows(const void* a, const void* b)
{
	const uint32_t* row_a = (const uint32_t*)a;
	const uint32_t* row_b = (const uint32_t*)b;

	const uint32_t x_a = row_a[0];
	const uint32_t y_a = row_a[1];
	const uint32_t z_a = row_a[2];
	const uint32_t w_a = row_a[3];

	const uint32_t x_b = row_b[0];
	const uint32_t y_b = row_b[1];
	const uint32_t z_b = row_b[2];
	const uint32_t w_b = row_b[3];

	if (x_a < x_b) return -1;
	if (x_a > x_b) return 1;

	if (y_a < y_b) return -1;
	if (y_a > y_b) return 1;

	if (z_a < z_b) return -1;
	if (z_a > z_b) return 1;

	if (w_a < w_b) return -1;
	if (w_a > w_b) return 1;

	return 0;
}

static void
glue_together(gluing_map_t* G,
			  const face_side_t fs1,
			  const face_side_t fs2)
{
	assert(G);
	G->data[fs1.face][fs1.side] = (face_side_t){ fs2.face, fs2.side };
	G->data[fs2.face][fs2.side] = (face_side_t){ fs1.face, fs1.side };
}

/// Performs sanity checks on the connectivity of the gluing
/// map. Aborts if anything is wrong.
static inline void
validate_gluing_map(const gluing_map_t G)
{
	for (uint32_t f = 0; f < G.face_count; ++f)
	{
		for (uint32_t s = 0; s < 3; ++s)
		{
			const face_side_t fs = {
				.face = f,
				.side = s
			};
			const face_side_t fs_other = other(G, fs);

			if (fs.face == fs_other.face &&
				fs.side == fs_other.side)
			{
				fprintf(stderr, "gluing map points face-side to itself [%d, %d]\n",
						fs.face, fs.side);
				abort();
			}

			const face_side_t fs_other_other = other(G, fs_other);
			if (fs.face != fs_other_other.face ||
				fs.side != fs_other_other.side)
			{
				fprintf(stderr, "gluing map is not involution (applying it twice does not return the original face-side) [%d, %d] -- [%d, %d] -- [%d, %d]\n",
						fs.face, fs.side, fs_other.face, fs_other.side, fs_other_other.face, fs_other_other.side);
				abort();
			}
		}
	}
}

static gluing_map_t
build_gluing_map(const adjacency_list_t F)
{
	uint32_t (*S)[4] = calloc(F.face_count * 3ull, sizeof(uint32_t[4]));
	assert(S);

	for (uint32_t f = 0; f < F.face_count; ++f)
	{
		for (uint32_t s = 0; s < 3; ++s)
		{
			const face_side_t fs = { .face = f, .side = s };
			const face_side_t fs_next = next_side(fs);

			const uint32_t i = F.data[fs.face][fs.side];
			const uint32_t j = F.data[fs_next.face][fs_next.side];

			S[f * 3 + s][0] = min(i, j);
			S[f * 3 + s][1] = max(i, j);
			S[f * 3 + s][2] = fs.face;
			S[f * 3 + s][3] = fs.side;
		}
	}

	qsort(S, F.face_count * 3ull, sizeof(uint32_t[4]), sort_rows);

	gluing_map_t G = {
		.data = calloc(F.face_count, sizeof(face_side_t[3])),
		.face_count = F.face_count
	};

	for (uint32_t p = 0; p < F.face_count * 3ull; p += 2)
	{
		const face_side_t fs0 = { .face = S[p + 0][2], .side = S[p + 0][3] };
		const face_side_t fs1 = { .face = S[p + 1][2], .side = S[p + 1][3] };
		glue_together(&G, fs0, fs1);
	}
	free(S);

	validate_gluing_map(G);

	return G;
}

/*************************************************************
 * Intrinsic Delaunay and edge flipping
 ************************************************************/

static face_side_t
flip_edge(adjacency_list_t* F, gluing_map_t* G,
		  lengths_array_t* l, const face_side_t s0)
{
	// Get the neighbouring face-side
	const face_side_t s1 = other(*G, s0);

	// Get the 3 sides of each face
	const face_side_t s2 = next_side(s0);
	const face_side_t s3 = next_side(s2);
	const face_side_t s4 = next_side(s1);
	const face_side_t s5 = next_side(s4);

	// Get the sides glued to each side of the diamond
	const face_side_t s6 = other(*G, s2);
	const face_side_t s7 = other(*G, s3);
	const face_side_t s8 = other(*G, s4);
	const face_side_t s9 = other(*G, s5);

	// Get vertex indices for the vertices of the diamond
	const uint32_t v0 = F->data[s0.face][s0.side];
	const uint32_t v1 = F->data[s2.face][s2.side];
	const uint32_t v2 = F->data[s3.face][s3.side];
	const uint32_t v3 = F->data[s5.face][s5.side];

	// Get the two faces from our face-sides
	const uint32_t f0 = s0.face;
	const uint32_t f1 = s1.face;

	// Get the original lengths of the outside edges of the diamond
	const double l2 = l->data[s2.face][s2.side];
	const double l3 = l->data[s3.face][s3.side];
	const double l4 = l->data[s4.face][s4.side];
	const double l5 = l->data[s5.face][s5.side];

	// Compute the length of the new edge
	const double new_length = diagonal_length(*G, *l, s0);

	// Update the adjacency list F
	F->data[f0][0] = v3; F->data[f0][1] = v2; F->data[f0][2] = v0;
	F->data[f1][0] = v2; F->data[f1][1] = v3; F->data[f0][2] = v1;

	// Update the gluing map G
	glue_together(G, (face_side_t) { f0, 0 }, (face_side_t) { f1, 0 });
	glue_together(G, (face_side_t) { f0, 1 }, s7);
	glue_together(G, (face_side_t) { f0, 2 }, s8);
	glue_together(G, (face_side_t) { f1, 1 }, s9);
	glue_together(G, (face_side_t) { f1, 2 }, s6);

	// Update the edge lengths
	// Note that even the edges we didn't flip have been
	// re-labeled, so we need to update those too
	l->data[f0][0] = new_length;
	l->data[f0][1] = l3;
	l->data[f0][2] = l4;

	l->data[f1][0] = new_length;
	l->data[f1][1] = l5;
	l->data[f1][2] = l2;

	return (face_side_t) { .face = f0, .side = 0 };
}

static void
flip_to_delaunay(adjacency_list_t* F, gluing_map_t* G,
				 lengths_array_t* l)
{
	assert(F && G && l);
	assert(F->data && G->data && l->data);

	deque_t to_process;
	deque_init(&to_process);

	for (uint32_t f = 0; f < F->face_count; ++f)
	{
		for (uint32_t s = 0; s < 3; ++s)
		{
			deque_append(&to_process, (face_side_t) { f, s });
		}
	}

	while (to_process.count > 0)
	{
		// Get the next face=side in the queue
		face_side_t fs = deque_pop(&to_process);

		// Check if it satisifies the Delaunay criterion
		if (!is_delaunay(*G, *l, fs))
		{
			// Flip the edge
			// Note that we need to update the current face-side fs,
			// because it is relabelled during the flip
			fs = flip_edge(F, G, l, fs);

			// Enqueue neighbours for processing, as they may have become non-Delaunay
			const face_side_t neighbours[4] = {
				next_side(fs),
				next_side(next_side(fs)),
				next_side(other(*G, fs)),
				next_side(next_side(other(*G, fs)))
			};
			for (uint32_t i = 0; i < 4; ++i)
			{
				deque_append(&to_process, neighbours[i]);
			}
		}
	}

	deque_destroy(&to_process);
}

int
main(int argc, char* argv[])
{
	fprintf(stdout, "Hello, World\n");

	vertices_t V = {
		.data = calloc(5, sizeof(vertex_t)),
		.vertex_count = 5
	};
	{
		V.data[0].x = 0.0f;
		V.data[0].y = 5.0f;
		V.data[0].z = 0.0f;

		V.data[1].x = 0.0f;
		V.data[1].y = 1.0f;
		V.data[1].z = -3.0f;

		V.data[2].x = -4.0f;
		V.data[2].y = 0.0f;
		V.data[2].z = 0.0f;

		V.data[3].x = 0.0f;
		V.data[3].y = 1.0f;
		V.data[3].z = 3.0f;

		V.data[4].x = 4.0f;
		V.data[4].y = 0.0f;
		V.data[4].z = 0.0f;
	}

	adjacency_list_t F = {
		.data = calloc(6, sizeof(uint32_t[3])),
		.face_count = 6
	};
	{
		F.data[0][0] = 0;
		F.data[0][1] = 1;
		F.data[0][2] = 2;

		F.data[1][0] = 0;
		F.data[1][1] = 2;
		F.data[1][2] = 3;

		F.data[2][0] = 0;
		F.data[2][1] = 3;
		F.data[2][2] = 4;

		F.data[3][0] = 0;
		F.data[3][1] = 4;
		F.data[3][2] = 1;

		F.data[4][0] = 1;
		F.data[4][1] = 4;
		F.data[4][2] = 2;

		F.data[5][0] = 2;
		F.data[5][1] = 4;
		F.data[5][2] = 3;
	}

	gluing_map_t G = build_gluing_map(F);
	lengths_array_t l = build_edge_lengths(V, F);

	fprintf(stdout, "Initial mesh:\n");
	fprintf(stdout, "  n_verts = %d\n  n_faces = %d\n", V.vertex_count, F.face_count);
	double area = surface_area(F, l);
	fprintf(stdout, "  surface area: %.17g\n", area);
	bool all_del = true;
	for (uint32_t f = 0; f < F.face_count; ++f)
	{
		for (uint32_t s = 0; s < 3; ++s)
		{
			const face_side_t fs = { .face = f, .side = s };
			const bool deldel = is_delaunay(G, l, fs);
			all_del &= deldel;
		}
	}
	fprintf(stdout, "  is Delaunay: %s\n", all_del ? "true" : "false");

	flip_to_delaunay(&F, &G, &l);

	fprintf(stdout, "After Delaunay flips:\n");
	fprintf(stdout, "  n_verts = %d\n  n_faces = %d\n", V.vertex_count, F.face_count);
	area = surface_area(F, l);
	fprintf(stdout, "  surface area: %.17g\n", area);
	all_del = true;
	for (uint32_t f = 0; f < F.face_count; ++f)
	{
		for (uint32_t s = 0; s < 3; ++s)
		{
			const face_side_t fs = { .face = f, .side = s };
			const bool deldel = is_delaunay(G, l, fs);
			all_del &= deldel;
		}
	}
	fprintf(stdout, "  is Delaunay: %s\n", all_del ? "true" : "false");

	return 0;
}

#pragma once

#include <stdint.h>

typedef struct vertex_t
{
	float x, y, z;
} vertex_t;

typedef struct vertices_t
{
	vertex_t *data;
	uint32_t vertex_count;
} vertices_t;

typedef struct face_side_t
{
	uint32_t face;
	uint32_t side;
} face_side_t;

typedef struct adjacency_list_t
{
	uint32_t (*data)[3];
	uint32_t face_count;
} adjacency_list_t;

typedef struct lengths_array_t
{
	double (*data)[3];
	uint32_t face_count;
} lengths_array_t;

typedef struct gluing_map_t
{
	face_side_t (*data)[3];
	uint32_t face_count;
} gluing_map_t;

typedef struct deque_index_t
{
	uint32_t array_index;
	uint32_t element_index;
} deque_index_t;

typedef struct deque_t
{
	deque_index_t _front, _back;

	face_side_t** arrays;
	uint32_t count;
	uint32_t capacity;

	uint32_t _array_size;
	uint32_t _array_count;
	uint32_t _array_capacity;
} deque_t;

void deque_init(deque_t* d);
void deque_destroy(deque_t* d);
void deque_append(deque_t* d, const face_side_t fs);
face_side_t deque_pop(deque_t* d);

#include <string.h>
#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#include "types.h"

static inline bool
_deque_can_increment(const deque_t* d, deque_index_t i)
{
	if (i.element_index + 1 < d->_array_size)
		return true;

	if (i.array_index + 1 < d->_array_capacity)
		return true;

	return false;
}

static inline void
_deque_increment(deque_t* d, deque_index_t* i)
{
	if (i->element_index + 1 < d->_array_size)
	{
		i->element_index++;
	}
	else
	{
		i->element_index = 0;
		i->array_index++;
	}
}

static inline void
_deque_grow(deque_t* d)
{
	if (d->capacity > d->count &&
		d->_back.array_index < d->_array_count &&
		_deque_can_increment(d, d->_back))
		return;

	face_side_t* face_sides = malloc(sizeof(face_side_t) * d->_array_size);

	if (d->_array_count >= d->_array_capacity)
	{
		const uint32_t capacity = d->_array_capacity == 0 ? 8 : d->_array_capacity * 2;
		face_side_t** array = malloc(capacity * sizeof(face_side_t*));
		if (d->arrays != NULL)
		{
			memcpy_s(array, capacity * sizeof(face_side_t*),
				d->arrays, d->_array_count * sizeof(face_side_t*));
			free(d->arrays);
		}
		d->arrays = array;
		d->_array_capacity = capacity;
	}

	d->arrays[d->_array_count] = face_sides;
	d->capacity += d->_array_size;
	d->_array_count++;
}

void
deque_init(deque_t* d)
{
	*d = (deque_t){
		._front = {
			.array_index = 0,
			.element_index = 0
		},
		._back = {
			.array_index = 0,
			.element_index = 0
		},

		.arrays = NULL,
		.count = 0,
		.capacity = 0,

		._array_size = 256,
		._array_count = 0,
		._array_capacity = 0,
	};
}

void
deque_destroy(deque_t* d)
{
	for (uint32_t i = 0; i < d->_array_count; ++i)
		free(d->arrays[i]);
	free(d->arrays);
}

void
deque_append(deque_t* d, const face_side_t fs)
{
	_deque_grow(d);
	d->arrays[d->_back.array_index][d->_back.element_index] = fs;
	_deque_increment(d, &d->_back);
	d->count++;
}

face_side_t
deque_pop(deque_t* d)
{
	d->count--;

	uint32_t array_index = d->_front.array_index;
	uint32_t element_index = d->_front.element_index;
	array_index += element_index / d->_array_size;
	element_index = element_index % d->_array_size;

	_deque_increment(d, &d->_front);

	const face_side_t fs = d->arrays[array_index][element_index];
	return fs;
}
